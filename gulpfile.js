var gulp = require('gulp'),
	stylus = require('gulp-stylus'),
	webpackStream = require('webpack-stream'),
	webpack = webpackStream.webpack;

gulp.task('stylus', function() {
	return gulp.src(['./css/*.styl'])
		.pipe(stylus({
			compress: false,
		}))
		.pipe(gulp.dest('./css'));
});

gulp.task('wp', function() {
	return gulp.src('home.js')
		.pipe(webpackStream({
			entry: "./home",
			output: {
			    filename: "build.js",
			    library: 'home',
			},
		}))
		.pipe(gulp.dest('./'));
});

gulp.task('watch', function() {
	gulp.watch(['./css/*.styl'], ['stylus']);
	gulp.watch(['./home.js'], ['wp']);
});